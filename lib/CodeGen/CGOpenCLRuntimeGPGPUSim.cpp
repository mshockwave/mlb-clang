//===----- CGOpenCLRuntime.cpp - Interface to OpenCL Runtimes -------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This provides an abstract class for OpenCL code generation.  Concrete
// subclasses of this implement code generation for specific OpenCL
// runtime libraries.
//
//===----------------------------------------------------------------------===//

#include "CGOpenCLRuntimeGPGPUSim.h"
#include "CodeGenFunction.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/GlobalValue.h"
#include <assert.h>

using namespace clang;
using namespace CodeGen;

llvm::Type *CGOpenCLRuntimeGPGPUSim::convertOpenCLSpecificType(const Type *T) {
  assert(T->isOpenCLSpecificType() &&
         "Not an OpenCL specific type!");

  llvm::LLVMContext& Ctx = CGM.getLLVMContext();
  uint32_t ImgAddrSpc =
    CGM.getContext().getTargetAddressSpace(LangAS::opencl_global);
  switch (cast<BuiltinType>(T)->getKind()) {
  default: 
    llvm_unreachable("Unexpected opencl builtin type!");
    return nullptr;
#define IMAGE_TYPE(ImgType, Id, SingletonId, Access, Suffix) \
  case BuiltinType::Id: \
    return llvm::PointerType::get( \
        llvm::StructType::create(Ctx, "opencl." #ImgType "_" #Suffix "_t"), \
        ImgAddrSpc);
#include "clang/Basic/OpenCLImageTypes.def"
  case BuiltinType::OCLSampler:
    return llvm::IntegerType::get(Ctx, 32);
  case BuiltinType::OCLEvent:
    return llvm::PointerType::get(llvm::StructType::create(
                           Ctx, "opencl.event_t"), 0);
  case BuiltinType::OCLClkEvent:
    return llvm::PointerType::get(
        llvm::StructType::create(Ctx, "opencl.clk_event_t"), 0);
  case BuiltinType::OCLQueue:{
    // Use integer type to represent queue_t for now
    return CGM.IntTy;
  }
  case BuiltinType::OCLNDRange:{
    /*
     struct {
      unsigned work_dim;
      unsigned int gwoff[3], gws[3], lws[3];
     }
     */
    auto* i32Ty = llvm::Type::getInt32Ty(Ctx);
    auto* i32Array3Ty = llvm::ArrayType::get(i32Ty, 3);
    return llvm::StructType::create(
      "opencl.ndrange_t",
      i32Ty,
      i32Array3Ty, i32Array3Ty, i32Array3Ty,
      nullptr
    );
  }
  case BuiltinType::OCLReserveID:
    return llvm::PointerType::get(
        llvm::StructType::create(Ctx, "opencl.reserve_id_t"), 0);
  }
}

CGOpenCLRuntimeGPGPUSim::~CGOpenCLRuntimeGPGPUSim(){}
