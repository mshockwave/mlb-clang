//===----- CGOpenCLRuntime.h - Interface to OpenCL Runtimes -----*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This provides an abstract class for OpenCL code generation.  Concrete
// subclasses of this implement code generation for specific OpenCL
// runtime libraries.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_CLANG_LIB_CODEGEN_CGGPGPUSIMOPENCLRUNTIME_H
#define LLVM_CLANG_LIB_CODEGEN_CGGPGPUSIMOPENCLRUNTIME_H

#include "clang/AST/Type.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Value.h"
#include "CGOpenCLRuntime.h"

namespace clang {

class VarDecl;

namespace CodeGen {

class CodeGenFunction;
class CodeGenModule;

class CGOpenCLRuntimeGPGPUSim : public CGOpenCLRuntime{

public:
  CGOpenCLRuntimeGPGPUSim(CodeGenModule &CGM) : CGOpenCLRuntime(CGM) {}
  llvm::Type *convertOpenCLSpecificType(const Type *T) override;
  ~CGOpenCLRuntimeGPGPUSim() override;
};

}
}

#endif
